#!/usr/bin/env perl
# The MIT License (MIT)
# 
# Copyright (c) 2011-2015 Ismaël Bouya http://www.normalesup.org/~bouya/
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# Feel like tipping/donating? https://www.immae.eu/licenses_and_tipping

use strict;
use File::Basename;

sub parse_config_file {
    my ($config_line, $Name, $Value, $Config);
    (my $File, $Config) = @_;
    if (!open (CONFIG, "$File")) {
        print "ERROR: Config file not found : $File";
        exit(0);
    }
    my $multiline = 0;

    while (<CONFIG>) {
        $config_line=$_;
        chop ($config_line);
        $config_line =~ s/^\s*//;
        $config_line =~ s/\s*$//;
        if ( ($config_line !~ /^#/) && ($config_line ne "") ){
            if ($multiline) {
              $$Config{$Name} =~ s/\\$//;
              $$Config{$Name} .= $config_line;
            } else {
              ($Name, $Value) = split (/\s*=\s*/, $config_line);
              $Value =~ s/^~/$ENV{"HOME"}/;
              $$Config{$Name} = $Value;
            }
            $multiline = ($$Config{$Name} =~ /\\$/);
        }
    }
    close(CONFIG);
}

my %Config;
&parse_config_file ($ENV{"HOME"}."/.gen_html_documents.rc", \%Config);

my $entete = $Config{"entete"};
my $avant  = $Config{"avant"};
my $milieu = $Config{"milieu"};
my $apres  = $Config{"apres"};
my $html   = $Config{"html"};

my @ignore = split (/\s*,\s*/, $Config{"ignore"});
my @ext    = split (/\s*,\s*/, $Config{"ext"});

my $documents = $Config{"documents"};
my $droot = $Config{"dossier_web"};

$Config{"dossiers"} =~ s/^"//;
$Config{"dossiers"} =~ s/"$//;
my %dossiers = split (/"?\s*,\s*"?/, $Config{"dossiers"});

my ($dossier,$description);

open F, ">".$milieu;

while(($dossier,$description) = each(%dossiers)) {
	opendir(DIR, $documents.$dossier) || warn $documents.$dossier." coulnd't be opened\n";
	my @content = grep {$_ !~ /^\.\.?$/} readdir(DIR);
	my @sorted = sort { lc($a) cmp lc($b) } @content;
	closedir(DIR);
	
	print F "<h3>".$description."</h3>\n";
	print F "<ul>\n";
	foreach my $subpath (grep { -d $documents.$dossier.'/'.$_} @sorted) {
		subparse($dossier,$subpath,4);
		}
	foreach my $file (grep { -f $documents.$dossier.'/'.$_} @sorted) {
		my($filename, $directories, $suffix) = fileparse($file,qr/\.[^\.]*/);
		next if(!in_array(\@ext,lc($suffix)));
		$filename =~ tr/_/ /;
		print F "<li><a href='".$droot.$dossier.'/'.$file."'>".$filename."</a></li>\n";
		}
	print F "</ul>\n";
	}

sub subparse {
	my $path = shift;
	my $dossier = shift;
	my $rang = shift;
	
	opendir(DIR, $documents.$path."/".$dossier);
	my @content = grep {$_ !~ /^\.\.?$/} readdir(DIR);
	my @sorted = sort { lc($a) cmp lc($b) } @content;
	closedir(DIR);

	return if(in_array(\@ignore,$dossier));
	my $dossierspace = $dossier;
	$dossierspace =~ tr/_/ /;
	print F "<li>".$dossierspace."\n";
	print F "<ul>\n";
	foreach my $subpath (grep { -d $documents.$path."/".$dossier.'/'.$_} @sorted) {
		subparse($path."/".$dossier,$subpath,$rang+1);
		}
	foreach my $file (grep { -f $documents.$path."/".$dossier.'/'.$_} @sorted) {
		my($filename, $directories, $suffix) = fileparse($file,qr/\.[^\.]*/);
		next if(!in_array(\@ext,lc($suffix)));
		$filename =~ tr/_/ /;
		print F "<li><a href='".$droot.$path.'/'.$dossier.'/'.$file."'>".$filename."</a></li>\n";
		}
	print F "</ul></li>\n";
	}

sub in_array {
	my ($arr,$search_for) = @_;
	my %items = map {$_ => 1} @$arr; # create a hash out of the array values
	return (exists($items{$search_for}))?1:0;
	}

close F;

exec "cat $entete $avant $milieu $apres 1> $html" or die "$!\n";
exec "chmod -R go=rX,u=rwX $documents"
